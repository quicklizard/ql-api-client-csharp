﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace quicklizard.api
{
    public class Client
    {
        private String host;
        private String key;
        private String secret;
        private Authentication authenticator;
        private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        ///<summary>constructor</summary>
        ///<param name="host">QL API host</param>
        ///<param name="apiKey">Your QL API key</param>
        ///<param name="apiSecret">Your QL API secret</param>
        public Client(String host, String apiKey, String apiSecret)
        {
            this.host = host;
            this.key = apiKey;
            this.secret = apiSecret;
            this.authenticator = new Authentication(this.secret);
        }

        ///<summary>Perform GET request to QL API</summary>
        ///<param name="endpoint">API endpoint. For example `/api/v2/products?page=1`</param>
        ///<returns>return API response by Dictionary with key 'body'</returns>
        public Dictionary<string, string> get(string endpoint)
        {
            Uri url = getURL(endpoint);
            string digest = this.authenticator.signRequest(url.AbsolutePath, url.Query.Remove(0, 1));
            return HTTP.get(this.host, url.PathAndQuery, this.key, digest);
        }

        ///<summery>Perform POST request to QL API</summery>
        ///<param name="endpoint">API endpoint. For example `/api/v2/products/bulk_create`</param>
        ///<param name="payload">API request JSON payload</param>
        ///<returns>return API response by Dictionary with key 'body'</returns>
        public Dictionary<string, string> post(string endpoint, JObject payload)
        {
            Uri url = getURL(endpoint);
            string digest = this.authenticator.signRequest(url.AbsolutePath, url.Query.Remove(0, 1), payload);
            return HTTP.post(this.host, url.PathAndQuery, payload, this.key, digest);
        }

        ///<summery>Perform PUT request to QL API</summery>
        ///<param name="endpoint">API endpoint. For example `/api/v2/products/bulk_create`</param>
        ///<param name="payload">API request JSON payload</param>
        ///<returns>return API response by Dictionary with key 'body'</returns>
        public Dictionary<string, string> put(string endpoint, JObject payload)
        {
            Uri url = getURL(endpoint);
            String digest = this.authenticator.signRequest(url.AbsolutePath, url.Query.Remove(0, 1), payload);
            return HTTP.put(this.host, url.PathAndQuery, payload, this.key, digest);
        }

        /**
       * Get API request URL from provided endpoint. Joins API host and endpoint and converts into java.net.URL instance
       * @param endpoint - API endpoint. For example `/api/v2/products/bulk_update`
       * @return API request URL
       */
        private Uri getURL(String endpoint)
        {
            String endpointWithTimestamp = endpointWithTs(endpoint);
            return new Uri(String.Format("{0}{1}", this.host, endpointWithTimestamp));
        }

        /**
         * Adds qts timestamp query-string parameter to API endpoint
         * @param endpoint API request endpoint
         * @return API request endpoint with qts parameter
         */
        private String endpointWithTs(String endpoint)
        {
            long ts = (long)(DateTime.UtcNow - UnixEpoch).TotalMilliseconds;
            if (endpoint.Contains("?"))
            {
                return String.Format("{0}&qts={1}", endpoint, ts);
            }
            else
            {
                return String.Format("{0}?qts={1}", endpoint, ts);
            }
        }
    }
}
