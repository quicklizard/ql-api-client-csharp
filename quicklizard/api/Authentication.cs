﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace quicklizard.api
{
    public class Authentication
    {
        private string apiSecret;

        ///<summary>
        ///constructor
        ///</summary>
        ///<param name="apiSecret">  QL API secret </param>
        public Authentication(string apiSecret)
        {
            this.apiSecret = apiSecret;
        }


        ///<summary>/Sign request using path and query string</summary>
        ///<param name="path">API request path</param>
        ///<param name="queryString">API request query-string</param>
        ///<returns>API request signature</returns>
        public string signRequest(string path, string queryString)
        {
            string rawDigest = String.Format("{0}{1}{2}", path, queryString, this.apiSecret);
            return getAPIDigest(rawDigest);
        }

        ///<summary>Sign request using path, query string and request body</summary>
        ///<param name="path">API request path</param>
        ///<param name="body"API request body></param>
        ///<param name="queryString"API request query-string></param>
        ///<returns>API request signature</returns>
        public string signRequest(string path, string queryString, JObject body)
        {
            string rawDigest = String.Format("{0}{1}{2}{3}", path, queryString, body, this.apiSecret);
            return getAPIDigest(rawDigest);
        }

        ///<summary>Create API request signature from raw string</summary>
        ///<param name="rawDigest"> raw string to convert to signature</param>
        ///<returns>API signature</returns>
        public string getAPIDigest(string rawDigest)
        {
            using (SHA256 hash = SHA256Managed.Create())
            {
                return string.Join("", hash
                  .ComputeHash(Encoding.UTF8.GetBytes(rawDigest))
                  .Select(item => item.ToString("x2")));
            }
        }
    }
}
