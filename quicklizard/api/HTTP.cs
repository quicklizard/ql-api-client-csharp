﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace quicklizard.api
{
    class HTTP
    {
        /**
       * Perform HTTP GET request
       * @param host request host
       * @param endpoint request path = '/api/v2/products/confirmed?page=1&period=15.mins&qts=1499072903981'
       * @param key API key
       * @param digest API request signature
       * @return HTTP response
       * @throws WebException
       */
        public static Dictionary<string, string> get(string host, string endpoint, string key, string digest)
        {
            try
            {
                string apiURL = getRequestURL(host, endpoint);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiURL);
                request.Method = "GET";
                request.Headers.Add("API_KEY", key);
                request.Headers.Add("API_DIGEST", digest);

                return sendRequest(request);
            }
            catch (Exception ex)
            {
                return new Dictionary<string, string>() { { "body", ex.Message } };
            }
        }

        /**
         * Perform HTTP POST request
         * @param host request host
         * @param endpoint request path = '/api/v2/products/confirmed?page=1&period=15.mins&qts=1499072903981'
         * @param payload request body as JSON
         * @param key API key
         * @param digest API request signature
         * @return HTTP response
         * @throws WebException
         */
        public static Dictionary<string, string> post(string host, string endpoint, JObject payload, string key, string digest)
        {
            try
            {
                string apiURL = getRequestURL(host, endpoint);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiURL);
                request.Method = "POST";
                request.ContentType = "application/json; charset=utf-8";
                request.Headers.Add("API_KEY", key);
                request.Headers.Add("API_DIGEST", digest);
                request.Accept = "application/json";

                using (StreamWriter streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(payload);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                return sendRequest(request);
            }
            catch (Exception ex)
            {
                return new Dictionary<string, string>() { { "body", ex.Message } };
            }
        }

        /**
         * Perform HTTP PUT request
         * @param host request host
         * @param endpoint request path = '/api/v2/products/confirmed?page=1&period=15.mins&qts=1499072903981'
         * @param payload request body as JSON
         * @param key API key
         * @param digest API request signature
         * @return HTTP response
         * @throws WebException
         */
        public static Dictionary<string, string> put(string host, string endpoint, JObject payload, string key, string digest)
        {
            try
            {
                string apiURL = getRequestURL(host, endpoint);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiURL);
                request.Method = "PUT";
                request.ContentType = "application/json";
                request.Headers.Add("API_KEY", key);
                request.Headers.Add("API_DIGEST", digest);
                request.Accept = "application/json";

                using (StreamWriter streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(payload);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                return sendRequest(request);
            }
            catch (Exception ex)
            {
                return new Dictionary<string, string>() { { "body", ex.Message } };
            }

        }

        /**
         * Get request URL from host and API endpoint (with query-string)
         * @param host request host
         * @param endpoint request endpoint = '/api/v2/products/confirmed?page=1&period=15.mins&qts=1499072903981'
         * @return request URL as string
         */
        private static string getRequestURL(string host, string endpoint)
        {
            return String.Format("{0}{1}", host, endpoint);
        }


        /**
         * Perform HTTP request (GET/POST/PUT)
         * @param request object
         * @return API response by Dictionary with key 'statusCode'vand 'body'
        */
        private static Dictionary<string, string> sendRequest(HttpWebRequest request)
        {
            Dictionary<string, string> dicRes = new Dictionary<string, string>() { { "statusCode", "" }, { "body", "" } };
            string txtRespons = string.Empty;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream stream = response.GetResponseStream())
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        txtRespons = reader.ReadToEnd();
                    }

                    dicRes["statusCode"] = response.StatusCode.GetHashCode().ToString();
                    dicRes["body"] = txtRespons;
                    return dicRes;
                }
            }
            catch (WebException ex)
            {
                HttpWebResponse webResponse = ex.Response as HttpWebResponse;
                dicRes["statusCode"] = webResponse.StatusCode.GetHashCode().ToString();
                dicRes["body"] = ex.Message;
                return dicRes;
            }
        }
    }
}
