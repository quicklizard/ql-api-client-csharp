﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using quicklizard.api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace quicklizard.api.Tests
{
    [TestClass()]
    public class AuthenticationTests
    {
        [TestMethod()]
        public void getAPIDigestTest()
        {
            string API_SECRET = "API_SECRET";
            Authentication authenticator = new Authentication(API_SECRET);
            string path = "/api/v2/products";
            string queryString = "page=1&qts=123456";
            string expected = "429fcfe520fb0c858b58e92836e9a812e609d3adcd9a46212be4bbcb987ac76f";
            string digest = authenticator.signRequest(path, queryString);
            Assert.AreEqual(expected, digest);
        }
    }
}