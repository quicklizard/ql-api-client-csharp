﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using quicklizard.api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace quicklizard.api.Tests
{
    [TestClass()]
    public class ClientTests
    {
        const string API_HOST = "https://rest.quicklizard.com";
        const string API_KEY = "API_KEY";
        const string API_SECRET = "API_SECRET";
        private Client apiClient = new Client(API_HOST, API_KEY, API_SECRET);
        private JObject jsonPayload = JObject.Parse(@"{payload:[{product_id: null}]}");
        private JObject invalidJSONPayload = JObject.Parse(@"{prod:[{product_id: null}]}");

        [TestMethod()]
        public void ClientGETTest()
        {
            try
            {
                Dictionary<string, string> response = apiClient.get("/api/v2/recommendations/all?page=1");
                Assert.AreEqual("200", response["statusCode"]);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod()]
        public void ClientPOSTWithValidPayloadTest()
        {
            try
            {
                Dictionary<string, string> response = apiClient.post("/api/v2/products/create", jsonPayload);
                Assert.AreEqual("200", response["statusCode"]);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod()]
        public void ClientPUTWithValidPayloadTest()
        {
            try
            {
                Dictionary<string, string> response = apiClient.put("/api/v2/products/update", jsonPayload);
                Assert.AreEqual("200", response["statusCode"]);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod()]
        public void ClientPOSTWithInvalidPayloadTest()
        {
            try
            {
                Dictionary<string, string> response = apiClient.post("/api/v2/products/create", invalidJSONPayload);
                Assert.AreEqual("500", response["statusCode"]);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod()]
        public void ClientPUTWithInvalidPayloadTest()
        {
            try
            {
                Dictionary<string, string> response = apiClient.put("/api/v2/products/update", invalidJSONPayload);
                Assert.AreEqual("500", response["statusCode"]);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
    }
}